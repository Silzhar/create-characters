import React from 'react';
import { uuid } from 'uuidv4';

import './App.css';
import Header from './Header/Header';
import Form from './components/Form/Form';
import CharacterList from './components/CharacterList'


class  App extends React.Component {
  state = {
    characterList: [
      {
        id: uuid(),
        name: 'Thrall Doomhammer',
        story: 'Thrall is the son of Durotan, former chieftain of the Frostwolf clan, and Draka. As a baby, he was found amongst the bloody bodies of his murdered parents by Aedelas Blackmoore, commander of the internment camps which held orcs after the end of the Second War.',
        imageUrl: 'https://a.wattpad.com/cover/73938357-288-k875632.jpg',
      },
      {
        id: uuid(),
        name: 'Jaina Proudmoore',
        story: 'Born into nobility, Jaina is the daughter of Grand Admiral Daelin Proudmoore, lord of the island nation of Kul Tiras. After displaying magical talent at a young age, it was arranged for Jaina to be sent to Dalaran to begin an apprenticeship with the Kirin Tor (the faction of mages who rule Dalaran).',
        imageUrl: 'https://i.pinimg.com/236x/9f/d7/12/9fd712b03af828fc873547d9a163e964.jpg',
      }
    ],
  };

  addCharater = (character) => {
    const newCharacter = {
      id: uuid(),
      ...character,
    }

    this.setState((prevState) => ({
      characterList: [...prevState.characterList, newCharacter ],
    }));
  }

  render(){
    console.log(this.state);
    return (
      <div className="App">
      <Header/>
      <Form onSubmitForm={this.addCharater} />
      <CharacterList characterList={this.state.characterList} />
       
      </div>
    );
  }
  
}

export default App;
