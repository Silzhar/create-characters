/* eslint-disable jsx-a11y/alt-text */
import React from 'react';

import './Header.css';

const imgUrlAliance = 
'https://ya-webdesign.com/transparent250_/alliance-crest-png-14.png';
const ImgUrlHorde = 
'https://www.lootware.de/wp-content/uploads/ABYTEX441-2-600x600.jpg'

class Header extends React.Component {

    render() {
        return(
            <header className='header'>
                <img src={ imgUrlAliance } className='header_aliance'/>
                <img src={ ImgUrlHorde } className='header_horde'/>
            </header>
        )
    }
}

export default Header;