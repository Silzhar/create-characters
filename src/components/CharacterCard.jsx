import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CharacterCard extends Component {
  render() {
    return (
      <div
      style={{
        border: '2px solid black',
        borderRadius: '0.25rem',
        padding: '1rem',
        width: '340px',
        height: '640px',
        margin: '0.5rem',
        }}
      >
          
        <h3>{this.props.name}</h3>

        <p>{this.props.story}</p>

        <img src={this.props.imageUrl} alt={this.props.name} />
      </div>
    );
  }
}

CharacterCard.propTypes = {
  name: PropTypes.string.isRequired,
  story: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
};

export default CharacterCard;
