import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CharacterCard from './CharacterCard'


class CharacterList extends Component {
  render() {
    return <div>
        <h2>Characters:</h2>
        <div style={{
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'center',
            flexWrap: 'wrap',
            
        }}>
            {this.props.characterList.map((character) => (
                <CharacterCard hey={character.id} 
                name={character.name}
                story={character.story}
                imageUrl={character.imageUrl}
                />
            ))}
        </div>
    </div>;
  }
}

CharacterList.propTypes = {
  characterList: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      story: PropTypes.string,
      imageUrl: PropTypes.string,
    })
  ).isRequired,
};

export default CharacterList;
