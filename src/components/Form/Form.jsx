/* eslint-disable react/no-direct-mutation-state */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { uuid } from 'uuidv4';

import './Form.css';

const orcUrl =
  'https://i.pinimg.com/236x/eb/0b/7c/eb0b7cef2210004f8bb79784854a165f--warcraft-orc-world-of-warcraft.jpg';
const femaleOrc =
  'https://i.imgur.com/BMBSiCr.jpg';
const undeadUrl = 
  'https://i.pinimg.com/236x/78/86/67/78866727961f776d8438e4c85bde7e5a.jpg';
const taurenUrl = 
  'https://vignette.wikia.nocookie.net/vanilla-wow/images/a/a1/Taurenew.png/revision/latest/scale-to-width-down/200?cb=20150413135712';
const trollUrl = 
  'https://www.kindpng.com/picc/m/28-289892_universal-fiction-stories-wiki-wow-vol-jin-png.png';
const elfUrl =
  'https://ya-webdesign.com/images600_/wow-png-night-elf-6.png';
const humanUrl = 
  'https://www.pngitem.com/pimgs/m/246-2468321_wow-png-human-world-of-warcraft-human-king.png';
const dwarfUrl =
  'https://i.pinimg.com/236x/a7/a2/61/a7a26133527306e6db51ecef7fc5ae4f--hunter-games-hunter-s.jpg';
const gnomeUrl =
  'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/d099d316-98cf-4095-a2f9-32cca4219baa/dam5yn3-bdd85dd1-365a-4118-b321-7112d158f8db.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvZDA5OWQzMTYtOThjZi00MDk1LWEyZjktMzJjY2E0MjE5YmFhXC9kYW01eW4zLWJkZDg1ZGQxLTM2NWEtNDExOC1iMzIxLTcxMTJkMTU4ZjhkYi5wbmcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.9NO0_lSS4u98FHZrL9VsXuVNfCwEJvjKspOuo85o-xE';


class Form extends Component {
  state = {
    faction: '',
    raceHorde: '',
    raceAliance: '',
    sex: '',
    id: uuid(),
    name: '',
    story: '',
    imageUrl: '',
  };

  // NO recargar la página.
  handleOnSubmit = (event) => {
    event.preventDefault();

    // Invocar la función del componente App y enviar estado de este componente.
    this.props.onSubmitForm(this.state);

    // Limpiar formulario.
    this.setState({
      faction: '',
      raceHorde: '',
      raceAliance: '',
      sex: '',
      name: '',
      story: '',
      imageUrl: '',
    });
  };

  // Hacer arrow para no perder la referencia a la clase.
  // Usar las propiedades "name" de los imput en los que se escribe.
  handleOnChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    // Para cambiar el estado.
    this.setState({
      [inputName]: inputValue,
    });
  };

  handleOnChangeFaction = (ev) => {
    const inputName = ev.target.name;
    const inputValue = ev.target.value;

    if (inputName === 'faction') {
      this.setState({
        [inputName]: inputValue,
        male: '',
        female: '',
      });
    }
    // Mantener el estado de facción al elegir raza.
    this.setState({
      [inputName]: inputValue,
    });

    if (inputName === 'sex') {
      this.setState({
        [inputName]: inputValue,
        horde: '',
        aliance: '',
      });
    }
    // Mantener el estado de raza al elegir sexo.
    this.setState({
      [inputName]: inputValue,
    });    
  };

  render() {
    return (
      <form onSubmit={this.handleOnSubmit}>
        <div className="faction">
          <div htmlFor="faction">
            <p>Choose your faction</p>
            <div>
              <input
                type="radio"
                name="faction"
                value="horde"
                checked={this.state.faction === 'horde'}
                onChange={this.handleOnChangeFaction}
              />
              <samp>Horde</samp>

              {this.state.faction === 'horde' ? (
                <div className="faction_race">
                  <p>Choose your race</p>
                  <div>
                    <input
                      type="radio"
                      name="raceHorde"
                      value="orc"
                      checked={this.state.raceHorde === 'orc'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Orcs</samp>
                    
                  </div>

                  <div>
                    <input
                      type="radio"
                      name="raceHorde"
                      value="undead"
                      checked={this.state.raceHorde === 'undead'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Undeads</samp>
                  </div>

                  <div>
                    <input
                      type="radio"
                      name="raceHorde"
                      value="tauren"
                      checked={this.state.raceHorde === 'tauren'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Taurens</samp>
                  </div>

                  <div>
                    <input
                      type="radio"
                      name="raceHorde"
                      value="troll"
                      checked={this.state.raceHorde === 'troll'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Trolls</samp>
                  </div>
                </div>

              ) : null}
              {this.state.raceHorde === 'orc' || this.state.sex === 'sex' ? (
                 
                 <div className="faction_race-sex">
                   <p>Choose your sex</p>
                   <div>
                     <input
                       type="radio"
                       name="sex"
                       value="male"
                       checked={this.state.sex === 'male'}
                       onChange={this.handleOnChangeFaction}
                       imageUrl={this.state.imageUrl = orcUrl}
                     />
                     <samp>Male</samp>
                     
                   </div>
 
                   <div>
                     <input
                       type="radio"
                       name="sex"
                       value="female"
                       checked={this.state.sex === 'female'}
                       onChange={this.handleOnChangeFaction}
                       imageUrl={this.state.imageUrl = femaleOrc}
                     />
                     <samp>Female</samp>
                   </div>
                 </div>
               
                 ) : null}

              
            </div>
            
            {/* {this.state.raceHorde === 'orc' ? (this.state.imageUrl = orcUrl) : null} */}
            {this.state.raceHorde === 'undead' ? (this.state.imageUrl = undeadUrl) : null}
            {this.state.raceHorde === 'tauren' ? (this.state.imageUrl = taurenUrl) : null}
            {this.state.raceHorde === 'troll' ? (this.state.imageUrl = trollUrl) : null}
            
            <div>
              <input
                type="radio"
                name="faction"
                value="aliance"
                checked={this.state.faction === 'aliance'}
                onChange={this.handleOnChangeFaction}
              />
              <samp>Aliance</samp>
              {this.state.faction === 'aliance' ? (
                <div className="faction_race">
                  <p>Choose your race</p>
                  <div>
                    <input
                      type="radio"
                      name="raceAliance"
                      value="elf"
                      checked={this.state.raceAliance === 'elf'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Elfs</samp>
                  </div>

                  <div>
                    <input
                      type="radio"
                      name="raceAliance"
                      value="human"
                      checked={this.state.raceAliance === 'human'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Humans</samp>
                  </div>

                  <div>
                    <input
                      type="radio"
                      name="raceAliance"
                      value="dwarf"
                      checked={this.state.raceAliance === 'dwarf'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Dwarfs</samp>
                  </div>

                  <div>
                    <input
                      type="radio"
                      name="raceAliance"
                      value="gnome"
                      checked={this.state.raceAliance === 'gnome'}
                      onChange={this.handleOnChangeFaction}
                    />
                    <samp>Gnomes</samp>
                  </div>
                </div>
              ) : null}
            </div>
            {this.state.raceAliance === 'elf' ? (this.state.imageUrl = elfUrl) : null}
            {this.state.raceAliance === 'human' ? (this.state.imageUrl = humanUrl) : null}
            {this.state.raceAliance === 'dwarf' ? (this.state.imageUrl = dwarfUrl) : null}
            {this.state.raceAliance === 'gnome' ? (this.state.imageUrl = gnomeUrl) : null} 
          </div>
        </div>

        {/* Unir label con input */}
        <label htmlFor="name">
          <p>Name:</p>
          <input
            type="text"
            name="name"
            placeholder="Character name"
            value={this.state.name}
            onChange={this.handleOnChange}
          />
        </label>

        <label htmlFor="story">
          <p>Story</p>
          <textarea
            name="story"
            placeholder="Character story"
            value={this.state.story}
            onChange={this.handleOnChange}
            cols="30"
            rows="6"
          ></textarea>
        </label>

        <label htmlFor="imageUrl">
          <p>Character</p>
          {/* <input
            name="imageUrl"
            placeholder="Character img"
            value={this.state.imageUrl}
            onChange={this.handleOnChange}
          /> */}
        </label>

        <div style={{ padding: '1rem' }}>
          {this.state.imageUrl ? (
            <img src={this.state.imageUrl} alt="Image default" width="220px" />
          ) : null}
        </div> 
        
        <div>
          <button type="submit">Create character</button>
        </div>
      </form>
    );
  }
}

Form.propTypes = {
  onSubmitForm: PropTypes.func.isRequired,
};

export default Form;
